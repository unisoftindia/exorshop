//
//  ViewController.swift
//  EXOR
//
//  Created by Chaithra on 24/05/16.
//  Copyright © 2016 EXOR. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var WebView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        let url = URL(string: "http://www.deepra.com/Exor1")
        let request=URLRequest(url: url!)
        WebView.loadRequest(request)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

